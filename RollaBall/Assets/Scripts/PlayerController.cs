﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI timerText;
    public GameObject winTextObject;
    public GameObject loseTextObject;

    private float movementX;
    private float movementY;
    
    private Rigidbody rb;
    private int count;

    private float timeStill = 30.0f;
    private float timeLeft = 32.0f;
  

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }


    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12)
        {
            winTextObject.SetActive(true);

            GameOver();
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY); 

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }

     private void Update()
     {
        timeStill -= Time.deltaTime;
        timerText.text = "Time: " + (timeStill.ToString("0"));
        if (timeStill <= 0)
            GameDone();

        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
            GameOver();
     }

     private void GameDone()
    {
        loseTextObject.SetActive(true);
    }

     private void GameOver()
    {
        SceneManager.LoadScene(0);
    }
}